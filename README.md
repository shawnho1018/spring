## This project is to document how to use Gitlab-CI on Spring Boot Kubernetes
Following these steps:
1. Download Spring Boot Template: curl https://start.spring.io/starter.tgz -d dependencies=webflux,actuator | tar -xzvf - 
2. Add .gitlab-ci.yaml as the CD Pipeline
3. Use Buildpack to realize the build/pack/push processes  
